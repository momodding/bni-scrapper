import time
import urllib
import getpass
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from bs4 import BeautifulSoup


class BNI(object):
	__url = 'https://ibank.bni.co.id/MBAWeb/FMB;jsessionid=0000XtXc5EpmkFXWd8oLzCqW8Rx:1a1li5kr5?page=Thin_SignOnRetRq.xml&MBLocale=bh'

	# def __init__(self):
	# 	self.formLogin()

	def test(self):
		return "Test call method"

	def formLogin(self, tipe_permintaan, username, password):
		''' webdriver : Chrome() Firefox() PhantomJS() '''
		# username = "*********"
		# password = getpass.getpass('Masukkan password: ')
		# password = '*********'
		# self.__driver = webdriver.PhantomJS()
		self.__option = webdriver.ChromeOptions()
		self.__option.add_argument("--incognito")
		self.__option.add_argument("headless")
		self.__driver = webdriver.Chrome(chrome_options=self.__option)
		self.__driver.wait = WebDriverWait(self.__driver, 3)
		self.__username = username
		self.__password = password
		result = self.authLogin(tipe_permintaan)
		return result

	def authLogin(self, permintaan):
		try:
			self.__driver.get(self.__url)
			username = self.__driver.wait.until(EC.presence_of_element_located((By.ID, "CorpId")))
			# password = self.__driver.wait.until(EC.element_to_be_clickable((By.ID, "PassWord")))
			password = self.__driver.wait.until(EC.presence_of_element_located((By.ID, "PassWord")))
			# loginBTN = self.__driver.wait.until(EC.element_to_be_clickable((By.NAME, "value(Login)")))
			loginBTN = self.__driver.find_element_by_xpath("//input[@value='Login']")
			username.send_keys(self.__username)
			password.send_keys(self.__password)
			# loginBTN.send_keys(webdriver.common.keys.Keys.SPACE)
			loginBTN.click()
			print("Logging in.... \n")

			try:
				if permintaan == 'balance':
					result = self.cekSaldo()
					self.logout()
				elif permintaan == 'Mutasi':
					result = self.cekMutasi()
				else:
					return "Tipe permintaan tidak ditemukan"
				self.logout()
				return result
			except:
				self.logout()
				alert = self.__driver.switch_to_alert()
				print(alert.text)
				alert.accept()
		except:
			self.logout()
			return "Session timeout. please login again after 5 minute"

	def logout(self):
		try:
			logout = self.__driver.wait.until(EC.presence_of_element_located((By.ID, "LogOut")))
			logout.click()
			finishLogout = self.__driver.find_element_by_xpath("//input[@value='Keluar']")
			finishLogout.click()
			print("Anda berhasil logout")
			# self.__driver.close()
		except TimeoutException:
			return "Session timeout. please login again after 5 minute"

	def cekMutasi(self):
		# self.__driver.switch_to.frame(self.__driver.find_element_by_xpath("//frame[@name=\"menu\"]"))
		# menuMutasi = self.__driver.wait.until(EC.presence_of_element_located((By.XPATH, "//a[@href=\"account_information_menu.htm\"]")))
		menuMutasi = self.__driver.find_element_by_xpath('//a//div[text()="REKENING"]')
		menuMutasi.click()
		menuInfoSaldo = self.__driver.find_element_by_xpath('//a//div[text()="MUTASI REKENING"]')
		menuInfoSaldo.click()
		menuRekening = self.__driver.find_element_by_xpath("//select[@name='MAIN_ACCOUNT_TYPE']/option[@value='OPR']")
		menuRekening.click()
		nextPeriode = self.__driver.find_element_by_id("AccountIDSelectRq")
		nextPeriode.click()
		periodeTrx = self.__driver.find_element_by_xpath("//select[@name='TxnPeriod']/option[@value='LastMonth']")
		periodeTrx.click()
		nextRekening = self.__driver.find_element_by_id("FullStmtInqRq")
		nextRekening.click()

		try:
			# mutasi = self.__driver.find_element_by_xpath("//div[@class='Commondiv']").text
			mutasi = self.__driver.find_element_by_xpath("//div[@class='Commondiv']")
			# print(mutasi)

			table_mutasi = BeautifulSoup(mutasi.get_attribute('innerHTML') ,"html.parser")
			# print(table_mutasi.prettify())
			hasil_mutasi = list()
			data = list()
			# data = dict()
			table_body = table_mutasi.find_all('table', "CommonTableClass")
			# print(table_body[5])
			x = 0
			for row in table_body:
				cols = row.find_all('span')
				for i in range(1,len(cols),2):
					# print(x)
					if cols[i].get_text() != '':
						data.append(cols[i].get_text())
						# print(i)
						if x % 6 == 0:
							hasil_mutasi.append(data.copy())
							data.clear()
					x += 1
			print("Tgl \t\t\tUraian/Trx \t\t\tTipe \t\tNominal \tSaldo")
			for item in range(1, len(hasil_mutasi)):
				print(hasil_mutasi[item][0] +"\t"+ hasil_mutasi[item][1]+"\t"+ hasil_mutasi[item][2]+"\t"+ hasil_mutasi[item][3]+"\t"+ hasil_mutasi[item][4])
			# self.showMutasi()
			return hasil_mutasi[1:]
		except:
			return "Tidak ada mutasi hari ini"

		# self.__driver.switch_to_default_content()

	def cekSaldo(self):
		try:
			menuMutasi = self.__driver.find_element_by_xpath('//a//div[text()="REKENING"]')
			menuMutasi.click()
			menuInfoSaldo = self.__driver.find_element_by_xpath('//a//div[text()="INFORMASI SALDO"]')
			menuInfoSaldo.click()
			menuRekening = self.__driver.find_element_by_xpath("//select[@name='MAIN_ACCOUNT_TYPE']/option[@value='OPR']")
			menuRekening.click()
			nextRekening = self.__driver.find_element_by_id("AccountIDSelectRq")
			nextRekening.click()
			requestRekening = self.__driver.find_element_by_id("BalInqRq")
			requestRekening.click()
			# saldo = self.__driver.find_element_by_xpath("//table[@id='BalanceDisplayTable']/tbody//tr[@id='Row1_1']//td[@id='Row1_1_column2']//span[@id='Row1_1']").text
			saldo = self.__driver.find_element_by_xpath("//table[2]/tbody//tr[last()]//td[2]").text
			print("Saldo BNI saat ini adalah %s"%saldo)
			# self.__driver.switch_to_default_content()
			return saldo
		except TimeoutException:
		    return "Session timeout. please login again after 5 minute"


if __name__ == "__main__":
	BNI()