from flask import Flask, jsonify
from flask import request
from scrap_core import *

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

# @app.route("/predict", methods=['POST'])
# def predict():
#     text = request.form['text']
#     model = request.form['model']
#     result, prob = predict_sample(model, text)
#     return jsonify({"Hasil Sentimen":result}, {"Nilai positive":prob[1]}, {"Nilai negative":prob[0]})

@app.route("/api/balance", methods=['POST'])
def balance():
    username = request.form['username']
    password = request.form['password']
    core = BNI()
    result = core.formLogin("balance", username, password)
    return jsonify(hasil=result)

@app.route("/api/mutation", methods=['POST'])
def mutation():
    username = request.form['username']
    password = request.form['password']
    core = BNI()
    result = core.formLogin("Mutasi", username, password)
    return jsonify(hasil=result)




if __name__ == '__main__':
    app.run(debug=True, port=8900)
