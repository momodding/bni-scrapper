Package :
- Python 3.6.4
- Flask
- BeautifulSoap4
- Selenium

Instalasi dan Penggunaan:
- Download chromedriver (http://chromedriver.chromium.org/downloads). Ekstrak file. Letakkan pada folder /usr/bin
- Install package menggunakan pip
- Clone repository
- cd {repository}
- Jalankan menggunakan "python3 app.py"
- Aplikasi ini menggunakan port 8900

Cara penggunaan: https://documenter.getpostman.com/view/5577786/S1M3uQjq